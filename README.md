# Nybian, the Modern Debian Blend

Nybian is a [Debian Blend](https://www.debian.org/blends/) aiming to provide
a modern Linux system experience.
